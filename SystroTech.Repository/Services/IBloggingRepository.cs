﻿using System;
using System.Collections.Generic;
using SystroTech.Repository.Entities;

namespace SystroTech.Repository.Services
{
    public interface IBloggingRepository : IDisposable
    {
        TModel Get<TModel>(int id) where TModel : class;
        IEnumerable<TModel> SelectAll<TModel>() where TModel : class;
        
        bool Insert<TModel>(TModel entity) where TModel : class;

        Post GetPost(int blogId);
    }
}