﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystroTech.Repository.Entities;

namespace SystroTech.Repository.Services
{
    // https://msdn.microsoft.com/ru-ru/library/b1yfkh5e(v=vs.100).aspx
    public class BloggingRepository : IBloggingRepository
    {
        public BloggingRepository()
        {
            _context = new BloggingEntities();
        }

        protected BloggingEntities _context;

        public IEnumerable<TModel> SelectAll<TModel>() where TModel : class
        {
            return _context.Set<TModel>().ToList();
        }

        public TModel Get<TModel>(int id) where TModel : class
        {
            return _context.Set<TModel>().Find(id);
        }

        public bool Insert<TModel>(TModel model) where TModel : class
        {
            if (model == null)
                return false;

            try
            {
                _context.Set<TModel>().Add(model);
                return _context.SaveChanges() >= 0;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Post> SelectAllPosts()
        {
            return _context.Posts.ToList();
        }

        public Post GetPost(int blogId)
        {
            return _context.Set<Post>().FirstOrDefault(p => p.BlogId == blogId);
        }

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // NVI
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_context != null)
                {
                    _context.Dispose();
                    _context = null;
                }
            }
        }

        #endregion
    }
}