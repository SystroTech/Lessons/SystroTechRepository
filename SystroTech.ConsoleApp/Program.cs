﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystroTech.Repository;
using SystroTech.Repository.Entities;
using SystroTech.Repository.Services;

namespace SystroTech.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (repository = new BloggingRepository())
            {
                //List<Post> posts = repository.SelectAllPosts().ToList();
                var items1 = repository.SelectAll<Post>().ToList();
                var items2 = repository.SelectAll<Blog>().ToList();
                
                Post p1 = repository.GetPost(1);

                //Blog blog = new Blog
                //{
                //    Url = @"www.test.com"
                //};

                var post = new Post
                {
                    BlogId = 2,
                    Content = "Content frm MIC Lesson.",
                    Title = "Mic"
                };
                bool inserted = repository.Insert(post);

            }
        }

        private static IBloggingRepository repository;
    }
}
